var drawing;
var firstPoint;
var secondPoint;
var history = [];
var removedHistory = [];
var originalViewCenter = new Point($(window).width()/2, $(window).height()/2);
var originalViewport = {width: $(window).width(), height: $(window).height()};
var currentViewport = {width: $(window).width(), height: $(window).height()};
var redColor = "#DB2828";
var greenColor = "#16ab39";
var yellowColor = "#eaae00";
var blackColor = "#27292a";
var blueColor = "#1678c2";
var tealColor = "#00B5AD";
var previousEvent;

var currentColor = tealColor;
var currentZoomLevel = 1;
var currentStrokeWidth = 6;

var toolPanning = new paper.Tool();
toolPanning.onMouseDown =  function (event) {
    pan_start = event.point;
}

toolPanning.onMouseDrag = function (event) {
    var a = pan_start.subtract(event.point);
    a = a.add(paper.view.center);
    paper.view.center = a
}

var saveAction = function(lastAction, leaveRemovedHistory){
	history.push(lastAction);
	if(!leaveRemovedHistory) removedHistory = [];
}

var undoAction = function(){
	if(history.length > 0){
		var deleted = history.pop();
		deleted.visible = false;
		removedHistory.push(deleted);
	}
}

var redoAction = function(){
	if(removedHistory.length > 0){
		var restored = removedHistory.pop();
		restored.visible = true;
		saveAction(restored,  true);
	}	
}

var getPanningOffset = function(){
	var panningOffset = originalViewCenter.subtract(paper.view.center);
	return panningOffset;
}

var toolPen = new paper.Tool();
toolPen.onMouseDown = function(event){
		console.log(event.point);
	    drawing = new Path({
		segments: [event.point],
		strokeColor: currentColor,
		strokeWidth: currentStrokeWidth
	});
}

toolPen.onMouseDrag = function(event){
	drawing.add(event.point);
}

toolPen.onMouseUp = function(event){
	saveAction(drawing);
}

var toolRectangle = new paper.Tool();
toolRectangle.onMouseDown = function(event){
	firstPoint = event.point;
}

toolRectangle.onMouseDrag = function(event){
	secondPoint = event.point;
	var trackingRect = new Path.Rectangle(firstPoint, secondPoint);
	trackingRect.strokeColor = currentColor;
	trackingRect.strokeWidth = currentStrokeWidth;
	trackingRect.removeOn({
		drag: true,
		down: true,
		up:true
	});
}

toolRectangle.onMouseUp = function(event){
	secondPoint = event.point;
	drawing = new Path.Rectangle(firstPoint, secondPoint);
	drawing.strokeColor = currentColor;
	drawing.strokeWidth = currentStrokeWidth;
	saveAction(drawing);
}

var toolCircle = new paper.Tool();
toolCircle.onMouseDown = function(event){
	firstPoint = event.point;
}

toolCircle.onMouseDrag = function(event){
	secondPoint = event.point;
	var distance = calculateDistance(firstPoint,secondPoint)
	var trackingCircle = new Path.Circle(firstPoint, distance);
	trackingCircle.strokeColor = currentColor;
	trackingCircle.strokeWidth = currentStrokeWidth;
	trackingCircle.removeOn({
		drag: true,
		down: true,
		up:true
	});
}

toolCircle.onMouseUp = function(event){
	secondPoint = event.point;
	var distance = calculateDistance(firstPoint,secondPoint);
	drawing = new Path.Circle(firstPoint, distance);
	drawing.strokeColor = currentColor;
	drawing.strokeWidth = currentStrokeWidth;
	saveAction(drawing);
}

var toolText = new paper.Tool();
toolText.onMouseDown = function(event){
	$("input").blur();
	drawElement(event);
}

var writeText = function(point, boxHeight, content){
	drawing = new PointText(point);
	drawing.justification = 'left';
	drawing.fillColor = currentColor;
	drawing.fontSize = currentStrokeWidth*5;
	drawing.content = content;
	drawing.position.y += boxHeight/currentZoomLevel;
	if(content.length > 0) saveAction(drawing);
}

var drawElement = function(event){   
	var top = event.event.clientY;
	var left = event.event.clientX;
	var textPosition = event.point;
	var viewportWidth = $( window ).width();
	var viewportHeight = $( window ).height();
	var textboxWidth = currentStrokeWidth*5*10;
	var textboxHeight = currentStrokeWidth*5 * currentZoomLevel;
	textboxWidth = viewportWidth - left;
	if(viewportHeight < (top + textboxHeight)) textboxHeight = viewportHeight - top;

	div = $("<input>")
	div.attr({type: "text", class: 'inputTextBox'});
	div.css("top", top);
	div.css("left", left);
	div.css("height", textboxHeight);
	div.css("width", textboxWidth);
	div.css("color", currentColor);
	div.css("font-size", textboxHeight);

	$("body").append(div);
	div.focus();

	div.blur(function(){
		writeText(textPosition, textboxHeight, div.val());
		div.remove();
	});	

	div.focusout(function(){
		writeText(textPosition, textboxHeight, div.val());
		div.remove();
	});	
}

var selectPenTool = function(){
	toolPen.activate();
}

var selectCircleTool = function(){
	toolCircle.activate();
}

var selectRectTool = function(){
	toolRectangle.activate();
}

var selectTextTool = function(){
	toolText.activate();
}

var selectPanningTool = function(){
	toolPanning.activate();
}

var restoreZoom = function(){
	view.zoom = 1;
	currentZoomLevel = 1;
}

var restorePanning = function(){
	paper.view.center = originalViewCenter;
}

function calculateDistance(firstPoint,secondPoint){
	var x1 = firstPoint.x;
	var y1 = firstPoint.y;
	var x2 = secondPoint.x;
	var y2 = secondPoint.y;
	
	var distance = Math.sqrt((Math.pow((x2-x1), 2))+(Math.pow((y2-y1), 2)));
	return distance;
}

$(function() {
	$('#pen').click(function(){
		selectPenTool();
		$('#blackboard').css("cursor", "crosshair");
		$('html').popup('remove popup');
	});

	$('#circle').click(function(){
		selectCircleTool();
		$('#blackboard').css("cursor", "crosshair");
		$('html').popup('remove popup');
	});

	$('#rectangle').click(function(){
		selectRectTool();
		$('#blackboard').css("cursor", "crosshair");
		$('html').popup('remove popup');
	});

	$('#text').click(function(){
		selectTextTool();
		$('#blackboard').css("cursor", "crosshair");
		$('html').popup('remove popup');
	});

	$('#hand').click(function(){
		selectPanningTool();
		$('#blackboard').css("cursor", "pointer");
		$('html').popup('remove popup');
	});

	$('#undo').click(function(){
		undoAction();
	});

	$('#redo').click(function(){
		redoAction();
	});

	$('#thrash').click(function(){
		$('#thrashModal').modal('show');
	});

	$('#thrashModal .approve.button').click(function(){
		project.activeLayer.removeChildren();
	});

	$('#warnReloading .approve.button').click(function(){
		location.reload();
	});

	$(window).on('beforeunload', function(event){
		event.preventDefault();
		return false;
	})

	$(window).unload(function(e){
		console.log(e);
		e.preventDefault();
	});

	$('#red').click(function(){
		currentColor = redColor;
	});

	$('#green').click(function(){
		currentColor = greenColor;	
	});

	$('#blue').click(function(){
		currentColor = blueColor;
	});

	$('#yellow').click(function(){
		currentColor = yellowColor;
	});

	$('#black').click(function(){
		currentColor = blackColor;
	});

	$('#teal').click(function(){
		currentColor = tealColor;
	});

	$('#center').click(function(){
		restorePanning();
		restoreZoom();
	});

	$(window).bind('mousewheel DOMMouseScroll', function(event){
		if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
			if(currentZoomLevel >= 1) currentZoomLevel = currentZoomLevel * 1.2;
			else currentZoomLevel = currentZoomLevel / 0.8;
			view.zoom = currentZoomLevel;			
		}
		else {
			if(currentZoomLevel > 1) currentZoomLevel = currentZoomLevel / 1.2;
			else currentZoomLevel = currentZoomLevel * 0.8;
			view.zoom = currentZoomLevel;
		}
		currentViewport.height = originalViewport.height/view.zoom;
		currentViewport.width = originalViewport.width/view.zoom;
		console.log("current viewport: " , currentViewport);
	});

	$('#strokeWidthSlider').range({
		min: 1,
		max: 100,
		start: 6,
		onChange: function(val) { currentStrokeWidth = val; }
	});

	$('html').popup({
				position : 'right center',
				target   : '#pen',
				title    : 'Choose a tool',
				content  : 'You have to select a drawing tool first',
				on: 'manual',
				variation: 'huge',
				transition: 'slide down',
				duration: 1000,
				closable: true,
				delay: {show: 1000, hide: 0}
			});

	var canvas = document.getElementById("blackboard");
	var ctx = canvas.getContext("2d");

function r(ctx, x, y, w, h, c) {
  ctx.beginPath();
  ctx.rect(x, y, w, h);
  ctx.strokeStyle = c;
  ctx.stroke();
}

r(ctx, 0, 0, 32, 32, "black");
r(ctx, 4, 4, 16, 16, "red");
r(ctx, 8, 8, 16, 16, "green");
r(ctx, 12, 12, 16, 16, "blue");

function dlCanvas() {
	var imgData=ctx.getImageData(0,0,canvas.width,canvas.height);
	var data=imgData.data;
	for(var i=0;i<data.length;i+=4){
    	if(data[i+3]<255){
			data[i]=255;
			data[i+1]=255;
			data[i+2]=255;
			data[i+3]=255;
    	}
	}
	ctx.putImageData(imgData,0,0);
  	var dt = canvas.toDataURL('image/png');
  	dt = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');
  	dt = dt.replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=multipainter.png');
  	this.href = dt;
};

document.getElementById("dl").addEventListener('click', dlCanvas, false);

  	$('html').popup('show');
});

